/* eslint-disable */
'use strict';

/**
 * Общие пакеты для сборки
 */
import gulp from 'gulp';
import gutil from 'gulp-util';
import plumber from 'gulp-plumber';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import rigger from 'gulp-rigger';

/**
 * Пакеты для сборки изображений
 */
import imagemin from 'gulp-imagemin';

/**
 * Пакеты для сборки js
 */
import browserify from 'browserify';
import babelify from 'babelify';
import uglify from 'gulp-uglify';

/**
 * Пакеты для сборки css
 */
import postcss from 'gulp-postcss';
import precss from 'precss';
import clearfix from 'postcss-clearfix';
import emMediaQuery from 'postcss-em-media-query';
import colorAlpha from 'postcss-color-alpha';
import pxToRem from 'postcss-pxtorem';
import autoprefixer from 'autoprefixer';

const browserSync = require('browser-sync').create();

/**
 * Пути к корневым собираемым файлам
 */
const dirs = {
  js: 'src/js/index.js',
  css: 'src/css/style.css',
  fonts: 'src/fonts/**/*',
  html: 'src/index.html',
  images: 'src/images/**/*'
};

/**
 * Задача для сборки css
 */
gulp.task('styles', () => {
  const processors = [
    precss,
    clearfix,
    pxToRem,
    colorAlpha,
    emMediaQuery,
    autoprefixer({browsers: ['last 3 version']}),
  ];

  return gulp.src(dirs.css)
          .pipe(plumber())
          .pipe(postcss(processors))
          .pipe(gulp.dest('build/css'));
});

/**
 * Задача для сборки js
 */
gulp.task('js', () => {
  return browserify({
          entries: dirs.js,
          extendions: ['.js', '.jsx', '.coffee'],
          debug: true
        })
        .transform('babelify', {
          presets: ['es2015', 'stage-0']
        })
        .bundle()
        .on('error', function(err) {
          gutil.log(gutil.colors.red.bold('[browserify error]'));
          gutil.log(err.message);
          this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('build/js'));
});

/**
 * Задача для сборки шрифтов
 */
gulp.task('fonts', () => {
  return gulp.src(dirs.fonts)
          .pipe(gulp.dest('build/'));
});

/**
 * Задача для сборки html
 */
gulp.task('html', () => {
  return gulp.src(dirs.html)
          .pipe(plumber())
          .pipe(rigger())
          .pipe(gulp.dest('build/'));
});

/**
 * Задача для сборки изображений
 */
gulp.task('images', () => {
  return gulp.src(dirs.images)
          .pipe(plumber())
          .pipe(imagemin({
            progressive: true,
            optimizationLevel: 3
          }))
          .pipe(gulp.dest('build/images/'));
});

/**
 * Задача для dev-сервера
 */
gulp.task('devServer', () => {
  browserSync.init({
    server: './build',
    port: 9002
  });

  browserSync.watch('build/**/*').on('change', browserSync.reload);
});

/**
 * Задача для отслеживания изменений в файлах
 */
gulp.task('watch', ['prod', 'devServer'], () => {
  gulp.watch('src/js/**/*.js', ['js']);
  gulp.watch('src/css/**/*.css', ['styles']);
  gulp.watch('src/**/*.html', ['html']);
  gulp.watch('src/fonts/**/*', ['fonts']);
  gulp.watch('src/images/**/*', ['images']);
});

/**
 * Задача для dev-разработки
 */
gulp.task('dev', ['watch']);

/**
 * Задача для production сборки
 */
gulp.task('prod', ['html', 'fonts', 'js', 'styles', 'images']);

/* eslint-enable */
